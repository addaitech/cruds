<?php
//including the database connection file
include_once("config.php");

//fetching data in descending order (lastest entry first)
//$result = mysql_query("SELECT * FROM users ORDER BY id DESC"); // mysql_query is deprecated
$result = mysqli_query($mysqli, "SELECT * FROM test ORDER BY id DESC"); // using mysqli_query instead
?>

<html>
<head>	
	<title>Homepage</title>
</head>

<body>
<a href="add.html">Add New Data</a><br/><br/>

	<table width='80%' border=0>

	<tr bgcolor='#CCCCCC'>
		<td>#</td>
		<td>Account</td>
		<td>Type</td>
		<td>Salutation</td>
		<td>First Name</td>
		<td>Last Name</td>
		<td>Title</td>
		<td>Email</td>
		<td>Address 1</td>
		<td>Address 2</td>
		<td>City</td>
		<td>State</td>
		<td>Zip</td>
		<td>Home Phone</td>
		<td>Cell Phone</td>
		<td>Work Phone</td>
		<td>Created</td>
	</tr>
	<?php 
	//while($res = mysql_fetch_array($result)) { // mysql_fetch_array is deprecated, we need to use mysqli_fetch_array 
	while($res = mysqli_fetch_array($result)) { 		
		echo "<tr>";
		echo "<td>".$res['account']."</td>";
		echo "<td>".$res['type']."</td>";
		echo "<td>".$res['salutation']."</td>";
		echo "<td>".$res['firstname']."</td>";
		echo "<td>".$res['lastname']."</td>";
		echo "<td>".$res['title']."</td>";
		echo "<td>".$res['email']."</td>";
		echo "<td>".$res['address1']."</td>";
		echo "<td>".$res['address2']."</td>";
		echo "<td>".$res['city']."</td>";
		echo "<td>".$res['zip']."</td>";
		echo "<td>".$res['homephone']."</td>";	
		echo "<td>".$res['cellphone']."</td>";
		echo "<td>".$res['workphone']."</td>";
		echo "<td>".$res['created']."</td>";
		echo "<td><a href=\"edit.php?id=$res[id]\">Edit</a> | <a href=\"delete.php?id=$res[id]\" onClick=\"return confirm('Are you sure you want to delete?')\">Delete</a></td>";		
	}
	?>
	</table>
</body>
</html>
