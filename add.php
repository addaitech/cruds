<html>
<head>
	<title>Add Data</title>
</head>

<body>
<?php
//including the database connection file
include_once("config.php");

$city = "";

if(isset($_POST['Submit'])) {	
	$account = mysqli_real_escape_string($mysqli, $_POST['account']);
	$type = mysqli_real_escape_string($mysqli, $_POST['type']);
	$salutation = mysqli_real_escape_string($mysqli, $_POST['salutation']);
	$firstname = mysqli_real_escape_string($mysqli, $_POST['firstname']);
	$lastname = mysqli_real_escape_string($mysqli, $_POST['lastname']);
	$title = mysqli_real_escape_string($mysqli, $_POST['title']);
	$email = mysqli_real_escape_string($mysqli, $_POST['email']);
	$address1 = mysqli_real_escape_string($mysqli, $_POST['address1']);
	$address2 = mysqli_real_escape_string($mysqli, $_POST['address2']);
	$city = mysqli_real_escape_string($mysqli, $_POST['city']);
	$state = mysqli_real_escape_string($mysqli, $_POST['state']);
	$zip = mysqli_real_escape_string($mysqli, $_POST['zip']);
	$homephone = mysqli_real_escape_string($mysqli, $_POST['homephone']);
	$cellphone = mysqli_real_escape_string($mysqli, $_POST['cellphone']);
	$workphone = mysqli_real_escape_string($mysqli, $_POST['workphone']);
	$created = mysqli_real_escape_string($mysqli, $_POST['created']);
		
	// checking empty fields
	if(empty($account) || empty($type) || empty($salutation) || empty($firstname) || empty($lastname) || empty($title)  || empty($email)  || empty($address1) || empty($address2) || empty($city) || empty($state) || empty($zip)  || empty($homephone) || empty($cellphone) || empty($workphone) || empty($created)) {
				
		if(empty($account)) {
			echo "<font color='red'>Account field is empty.</font><br/>";
		}
		
		if(empty($type)) {
			echo "<font color='red'>Type field is empty.</font><br/>";
		}
		
		if(empty($salutation)) {
			echo "<font color='red'>Salutation field is empty.</font><br/>";
		}

		if(empty($firstname)) {
			echo "<font color='red'>First name field is empty.</font><br/>";
		}
		
		if(empty($lastname)) {
			echo "<font color='red'>Last name field is empty.</font><br/>";
		}

		if(empty($title)) {
			echo "<font color='red'>Title field is empty.</font><br/>";
		}

		if(empty($email)) {
			echo "<font color='red'>Email field is empty.</font><br/>";
		}

		if(empty($address1)) {
			echo "<font color='red'>Address 1 field is empty.</font><br/>";
		}

		if(empty($address2)) {
			echo "<font color='red'>Address 2 field is empty.</font><br/>";
		}

		if(empty($city)) {
			echo "<font color='red'>City field is empty.</font><br/>";
		}

		if(empty($state)) {
			echo "<font color='red'>State field is empty.</font><br/>";
		}

		if(empty($zip)) {
			echo "<font color='red'>Zip field is empty.</font><br/>";
		}

		if(empty($homephone)) {
			echo "<font color='red'>Home phone field is empty.</font><br/>";
		}

		if(empty($cellphone)) {
			echo "<font color='red'>Cell phone field is empty.</font><br/>";
		}

		if(empty($workphone)) {
			echo "<font color='red'>Work phone field is empty.</font><br/>";
		}

		if(empty($created)) {
			echo "<font color='red'>Created field is empty.</font><br/>";
		}
		//link to the previous page
		echo "<br/><a href='javascript:self.history.back();'>Go Back</a>";
	} else { 
		// if all the fields are filled (not empty) 
			
		//insert data to database	
		$result = mysqli_query($mysqli, "INSERT INTO test (account, type, salutation, firstname, lastname, title, email, address1, address2, city, state, zip, homephone, cellphone, workphone, created) VALUES('$account','$type','$salutation', '$firstname', '$lastname', '$title', '$email', '$address1', '$address2', '$city', '$state', '$zip', '$homephone', '$cellphone', '$workphone', '$created')");
		
		//display success message
		echo "<font color='green'>Data added successfully.";
		echo "<br/><a href='index.php'>View Result</a>";
	}
}
?>
</body>
</html>
