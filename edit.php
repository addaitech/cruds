<?php
// including the database connection file
include_once("config.php");

if(isset($_POST['update']))
{	

	$id = mysqli_real_escape_string($mysqli, $_POST['id']);
	$account = mysqli_real_escape_string($mysqli, $_POST['account']);
	$type = mysqli_real_escape_string($mysqli, $_POST['type']);
	$salutation = mysqli_real_escape_string($mysqli, $_POST['salutation']);
	$firstname = mysqli_real_escape_string($mysqli, $_POST['firstname']);
	$lastname = mysqli_real_escape_string($mysqli, $_POST['lastname']);
	$title = mysqli_real_escape_string($mysqli, $_POST['title']);
	$email = mysqli_real_escape_string($mysqli, $_POST['email']);
	$address1 = mysqli_real_escape_string($mysqli, $_POST['address1']);
	$address2 = mysqli_real_escape_string($mysqli, $_POST['address2']);
	$city = mysqli_real_escape_string($mysqli, $_POST['city']);
	$state = mysqli_real_escape_string($mysqli, $_POST['state']);
	$zip = mysqli_real_escape_string($mysqli, $_POST['zip']);
	$homephone = mysqli_real_escape_string($mysqli, $_POST['homephone']);
	$cellphone = mysqli_real_escape_string($mysqli, $_POST['cellphone']);
	$workphone = mysqli_real_escape_string($mysqli, $_POST['workphone']);
	$created = mysqli_real_escape_string($mysqli, $_POST['created']);
	
	// checking empty fields
	if(empty($account) || empty($type) || empty($salutation) || empty($firstname) || empty($lastname) || empty($title) || empty($email) || empty($address1) || empty($address2) || empty($city) || empty($state) || empty($zip) || empty($homephone) || empty($cellphone) || empty($workphone) || empty($created)) {
				
		if(empty($account)) {
			echo "<font color='red'>Account field is empty.</font><br/>";
		}
		
		if(empty($type)) {
			echo "<font color='red'>Type field is empty.</font><br/>";
		}
		
		if(empty($salutation)) {
			echo "<font color='red'>Salutation field is empty.</font><br/>";
		}

		if(empty($firstname)) {
			echo "<font color='red'>First name field is empty.</font><br/>";
		}
		
		if(empty($lastname)) {
			echo "<font color='red'>Last name field is empty.</font><br/>";
		}

		if(empty($title)) {
			echo "<font color='red'>Title field is empty.</font><br/>";
		}

		if(empty($email)) {
			echo "<font color='red'>Email field is empty.</font><br/>";
		}

		if(empty($address1)) {
			echo "<font color='red'>Address 1 field is empty.</font><br/>";
		}

		if(empty($address2)) {
			echo "<font color='red'>Address 2 field is empty.</font><br/>";
		}

		if(empty($city)) {
			echo "<font color='red'>City field is empty.</font><br/>";
		}

		if(empty($state)) {
			echo "<font color='red'>State field is empty.</font><br/>";
		}

		if(empty($zip)) {
			echo "<font color='red'>Zip field is empty.</font><br/>";
		}

		if(empty($homephone)) {
			echo "<font color='red'>Home phone field is empty.</font><br/>";
		}

		if(empty($cellphone)) {
			echo "<font color='red'>Cell phone field is empty.</font><br/>";
		}

		if(empty($workphone)) {
			echo "<font color='red'>Work phone field is empty.</font><br/>";
		}

		if(empty($created)) {
			echo "<font color='red'>Created field is empty.</font><br/>";
		}		
	} else {	
		//updating the table
		//$result = mysqli_query($mysqli, "UPDATE users SET name='$name',age='$age',email='$email' WHERE id=$id");
		$result = mysqli_query($mysqli, "INSERT INTO test(account, type, salutation, firstname, lastname, title, email, address1, address2, city, state, zip, homephone, cellphone, workphone, created) VALUES('$account','$type','$salutation', '$firstname', '$lastname', '$title', '$email', '$address1', '$address2', '$city', '$state', '$zip', '$homephone', '$cellphone', '$workphone', '$created')");
		//redirectig to the display page. In our case, it is index.php
		header("Location: index.php");
	}
}
?>
<?php
//getting id from url
$id = $_GET['id'];

//selecting data associated with this particular id
$result = mysqli_query($mysqli, "SELECT * FROM test WHERE id=$id");

while($res = mysqli_fetch_array($result))
{
	$account = $res['account'];
	$type = $res['type'];
	$salutation = $res['salutation'];
	$firstname = $res['firstname'];
	$lastname = $res['lastname'];
	$title = $res['title'];
	$email = $res['email'];
	$address1 = $res['address1'];
	$address2 = $res['address2'];
	$city = $res['city'];
	$state = $res['state'];
	$zip = $res['zip'];
	$homephone = $res['homephone'];
	$cellphone = $res['cellphone'];
	$workphone = $res['workphone'];
	$created = $res['created'];


}
?>
<html>
<head>	
	<title>Edit Data</title>
</head>

<body>
	<a href="index.php">Home</a>
	<br/><br/>
	
	<form name="form1" method="post" action="edit.php">
		<table border="0">
			<tr> 
				<td>Account</td>
				<td><input type="text" name="account" value="<?php echo $account;?>"></td>
			</tr>
			<tr> 
				<td>Type</td>
				<td><input type="text" name="type" value="<?php echo $type;?>"></td>
			</tr>
			<tr> 
				<td>Salutation</td>
				<td><input type="text" name="salutation" value="<?php echo $salutation;?>"></td>
			</tr>
			<tr> 
				<td>First name</td>
				<td><input type="text" name="firstname" value="<?php echo $firstname;?>"></td>
			</tr>
			<tr> 
				<td>Last name</td>
				<td><input type="text" name="lastname" value="<?php echo $lastname;?>"></td>
			</tr>
			<tr> 
				<td>Title</td>
				<td><input type="text" name="title" value="<?php echo $title;?>"></td>
			</tr>
			<tr> 
				<td>Email</td>
				<td><input type="text" name="email" value="<?php echo $email;?>"></td>
			</tr>
			<tr> 
				<td>Address 1</td>
				<td><input type="text" name="address1" value="<?php echo $address1;?>"></td>
			</tr>
			<tr> 
				<td>Address 2</td>
				<td><input type="text" name="address2" value="<?php echo $address2;?>"></td>
			</tr>
			<tr> 
				<td>City</td>
				<td><input type="text" name="city" value="<?php echo $city;?>"></td>
			</tr>
			<tr> 
				<td>State</td>
				<td><input type="text" name="state" value="<?php echo $state;?>"></td>
			</tr>
			<tr> 
				<td>Zip</td>
				<td><input type="text" name="zip" value="<?php echo $zip;?>"></td>
			</tr>
			<tr> 
				<td>Home Phone</td>
				<td><input type="text" name="homephone" value="<?php echo $homephone;?>"></td>
			</tr>
			<tr> 
				<td>Cell Phone</td>
				<td><input type="text" name="cellphone" value="<?php echo $cellphone;?>"></td>
			</tr>
			<tr> 
				<td>Work Phone</td>
				<td><input type="text" name="workphone" value="<?php echo $workphone;?>"></td>
			</tr>
			<tr> 
				<td>Created</td>
				<td><input type="text" name="created" value="<?php echo $created;?>"></td>
			</tr>
			<tr>
				<td><input type="hidden" name="id" value=<?php echo $_GET['id'];?>></td>
				<td><input type="submit" name="update" value="Update"></td>
			</tr>
		</table>
	</form>
</body>
</html>
